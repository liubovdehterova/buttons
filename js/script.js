const showPasswordIcons = document.querySelectorAll('.show-password');
const hidePasswordIcons = document.querySelectorAll('.hide-password');
const passwordInputs = document.querySelectorAll('input[type="password"]');
const btn = document.querySelector('.btn');

//Показати пароль
showPasswordIcons.forEach((icon, index) => {
    icon.addEventListener('click', () => {
        passwordInputs[index].type = 'text';
        showPasswordIcons[index].style.display = 'none';
        hidePasswordIcons[index].style.display = 'block';
    });
});

//Приховати пароль
hidePasswordIcons.forEach((icon, index) => {
    icon.addEventListener('click', () => {
        passwordInputs[index].type = 'password';
        showPasswordIcons[index].style.display = 'block';
        hidePasswordIcons[index].style.display = 'none';
    });
});

//Натиснувши кнопку Підтвердити, порівнюю введені значення в полях input
btn.addEventListener('click', (e) => {
    e.preventDefault(); //відміняємо дію браузера по замовченю
    const password1 = passwordInputs[0].value; //значення 1 input
    const password2 = passwordInputs[1].value; //значення 2 input

    //Перевіряємо чи значення однакові і не поля не пусті
    if (password1 === password2 && password1 !== '' && password2 !== '') {
        alert('You are welcome');
    } else {
        //Створюємо попередження якщо паролі неоднакові, або поля пусті
        const additionalElement = document.createElement('p');
        additionalElement.textContent = 'Потрібно ввести однакові значення і щоб поля не були пусті';
        additionalElement.classList.add('writing');
        additionalElement.style.cssText = "color: red; margin-top: 0;";
        const submitButton = document.querySelector('.btn');
        submitButton.parentNode.insertBefore(additionalElement, submitButton);
    }
});